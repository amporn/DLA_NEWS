//
//  ViewController.swift
//  DLA News
//
//  Created by Aom on 3/7/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_index:Int32)
}

class MenuViewController: UIViewController , UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableMenu: UITableView!
    
      @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    var arrayMenuOption = [Dictionary<String,String>]()
    
    var btnMenu : UIButton!
    
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableMenu.tableFooterView = UIView()
        
        
    }
  
    func updateArrayMenuOptions()  {
        
        arrayMenuOption.append(["title":"แผนที่ข่าว สถ."])
        arrayMenuOption.append(["title":"ข่าวทั่วไป"])
        arrayMenuOption.append(["title":"เหตุการณ์สำคัญเร่งด่วน"])
        arrayMenuOption.append(["title":"ลงชื่อออก"])
        
        tableMenu.reloadData()
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
      
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(_index: index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell : UITableViewCell = tableMenu.dequeueReusableCell(withIdentifier: "cellMenu")!
        
        let lbTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        lbTitle.text = arrayMenuOption[indexPath.row]["title"]!
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOption.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateArrayMenuOptions()
    }
    
}

