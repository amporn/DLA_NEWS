//
//  LoginViewController.swift
//  DLA News
//
//  Created by Aom on 3/7/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var txtUser: UITextField!
    
    
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func callServiceLogin(username:String, password:String)  {
        
        ConnectLogin.servicesLogin(username: username, password: password, success:  { (response) -> Void in
            
            //            let json = JSON(data:(response?.data!)!)
            
            //            print("login json " ,json)
            
            //              success(response.data as AnyObject?)
            
        }) { (message) -> Void in
            //
            //            let alertVC = Alert.alertWith(title: "เข้าสู่ระบบผิดพลาด", message: "ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง", buttonTilte: "OK")
            ////            self.present(alertVC, animated:true, completion:nil)
            
            let alert = UIAlertController(title: "เข้าสู่ระบบผิดพลาด", message: "ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
        
    }
    
    @IBAction func pressSignIn(_ sender: Any) {
        
        if txtUser.text == "" || txtPassword.text == "" {
            
            //            let alert = UIAlertController(title: "เข้าสู่ระบบผิดพลาด", message: "ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง", preferredStyle: UIAlertControllerStyle.alert)
            //            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            //            self.present(alert, animated: true, completion: nil)
            
            let alertVC = Alert.alertWith(title: "เข้าสู่ระบบผิดพลาด", message: "ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง", buttonTilte: "OK")
            self.present(alertVC, animated:true, completion:nil)
            
        } else {
            
            callServiceLogin(username: txtUser.text!, password: txtPassword.text!)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MainNavigationId")
            self.present(controller, animated: true, completion: nil)
        }
    }
    
}





































