//
//  ConnectLogin.swift
//  DLA News
//
//  Created by Aom on 3/8/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ConnectLogin: NSObject {
    
    class func servicesLogin(username:String, password:String, success: @escaping SuccessHandler,fail: FailHandler) {
        
        let parametersLogin: [String: String] = [
            "cmd": "login",
            "login" : username ,
            "password" : password
        ]
        
        Alamofire.request(apiLogin, method: .get, parameters:parametersLogin ).responseJSON { response  in
            
            print("login  response.result " , response.result)
            print("login  response.request " ,response.request)
            print("login  response.data " ,response.data)
            
            let json = JSON(data:response.data!)
            
              success(response.data as AnyObject?)
            if let isOK: Int = json["status"].int {
                
                if isOK == 1 {
                    
                    success(response.data as AnyObject?)
                    
                    print("road response.data list " ,response.data)
                    
                } else {
                    
                    print("error ")
                }
                
            } else {
                
                print("error ")
            }
            
            print("login json " ,json)
            
        }
        
    }
    
    
}
