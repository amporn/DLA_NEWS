//
//  NewsTableViewCell.swift
//  DLA News
//
//  Created by Aom on 3/10/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbDate: UILabel!
    
    @IBOutlet weak var imgNews: UIImageView!
    
    @IBOutlet weak var lbTopic: UITextView!
    
     @IBOutlet weak var txtDetail: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
