//
//  Alert.swift
//  DLA News
//
//  Created by Aom on 3/8/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import Foundation
import UIKit

class Alert:NSObject {
    
    //alertWith
    class func alertWith(title:String, message:String, buttonTilte:String) -> UIAlertController {
        
        let alertVC : UIAlertController = UIAlertController(title: title, message:message, preferredStyle:UIAlertControllerStyle.alert)
        
        let defaultAction:UIAlertAction = UIAlertAction(title:buttonTilte, style:UIAlertActionStyle.cancel, handler: { (action:UIAlertAction) -> Void in
            
            
        })
        
        alertVC.addAction(defaultAction)
        
        return alertVC
    }
}


//let alert = UIAlertController(title: "Confirmation", message: "Change PIN will clear your current saved PIN \n Do you want to Change PIN?", preferredStyle: UIAlertControllerStyle.alert)
//alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
//alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler: {
//    
//    (action:UIAlertAction!) in print("you have pressed the Cancel button")
//}))
//
//self.present(alert, animated: true, completion: nil)
