//
//  ConnectNews.swift
//  DLA News
//
//  Created by Aom on 3/10/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ConnectGeneral:NSObject {  //general
    
    class func servicesGeneral (success: @escaping SuccessHandler,fail:@escaping FailHandler) {
        
        Alamofire.request(apiNews+"&type=2", method: .get, parameters: nil).responseJSON { response in
            
            let json = JSON(data: (response.data! as NSData) as Data)
            
            appendGeneralListData(json: json)
            
            if let isOK: Int = json["ok"].int {
                
                if isOK == 1 {
                    
                    success(response.data as AnyObject?)
                    
                    print("road response.data list " ,response.data)
                    
                } else {
                    
                    print("error ")
                }
                
            } else {
                
                print("error ")
            }
            
        }
    }
    
    class func appendGeneralListData(json:JSON) {
        
        if let newListJson:Array<JSON> = json["mobileNewsList"].arrayValue {
            
            for jsonData in newListJson {
                
                jsonGeneralList.append(jsonData)
                
                print("jsonNewsList ",jsonGeneralList)
                
            }
        } else {
            print("newListJson : \(json["newsList"].error)")
        }
        
        
    }
    
}


























