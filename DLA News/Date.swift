//
//  Date.swift
//  DLA News
//
//  Created by Aom on 3/16/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit

class Date: NSObject {
  
   class func convertDatetime(dateString : String) -> String! {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateObjTime = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "MMM dd, YYYY"
        return dateFormatter.string(from: dateObjTime!)
    }
    
}
























