
//
//  DetialViewController.swift
//  DLA News
//
//  Created by Aom on 3/14/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit
import SDWebImage

class DetialViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tbDetial:UITableView!
    
    @IBOutlet weak var lbDate: UILabel!
    
    @IBOutlet weak var txtTopic: UITextView!
    
    @IBOutlet weak var imgLastest: UIImageView!
    
    var topic = ""
    var detial = ""
    var lastest = ""
    var date = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtTopic.text = topic
        lbDate.text = date
        let imgURL = NSURL(string:lastest)
        imgLastest.sd_setImage(with: imgURL as URL?, placeholderImage: UIImage(named:"placeholder"))
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:NewsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewsTableViewCell
        cell.txtDetail.text = detial
        return cell
    }
    
}
