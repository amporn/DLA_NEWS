//
//  ApiConfig.swift
//  DLA News
//
//  Created by Aom on 3/8/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

typealias SuccessHandler = (_ res: AnyObject?) -> Void
typealias FailHandler = (_ message: String, _ error: AnyObject) -> Void

//  http://intranet.dla.go.th/download/newsImage/20170112/0F028E65-9233-27F3-BBD4-A53F2D2B3674_C3322D82DFFF1C06DAA581C1D3B7800.jpg
let IMAGE_URL = "http://intranet.dla.go.th/download/"


// Login
//http://dpm.depth1st.com/portal/mobile?cmd=login&login=1234567890000&password=123456
//http://intranet.dla.go.th/mobile
let apiLogin = "http://intranet.dla.go.th/mobile"
var jsonLogin = [JSON]()

//News
var jsonGeneralList = [JSON]()
var apiNews = "http://intranet.dla.go.th/mobile?cmd=listMobileNews" 

//Fast 
var jsonFastList = [JSON]()

var jsonNewsList  = [JSON]()
