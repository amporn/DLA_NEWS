//
//  FastNewViewController.swift
//  DLA News
//
//  Created by Aom on 3/10/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit
import SDWebImage

class FastNewViewController: BaseViewController , UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableFast: UITableView!
    
    var newTbCellNib = UINib(nibName:"NewTableviewCell", bundle:nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSlideMenuButton()
        callServiceFast()
        self.tableFast.register(newTbCellNib, forCellReuseIdentifier: "NewTableviewCell")
    }
    
    func callServiceFast() {
        
        ConncetFast.servicesFast(success: {(response) -> Void in
            
            self.tableFast.reloadData()
            
        }) {(message) -> Void in
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonFastList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:NewsTableViewCell = tableFast.dequeueReusableCell(withIdentifier: "NewTableviewCell") as! NewsTableViewCell
        
        let newsData = jsonFastList[indexPath.row]
        
        var imagePath = IMAGE_URL
        
        if let lastesImage = newsData["lastestImage"].string {
            
            imagePath += lastesImage
            let imgURL = NSURL(string:imagePath)
            
            cell.imgNews.sd_setImage(with: imgURL as URL?, placeholderImage: UIImage(named:"placeholder"))
        }
        else {
            
        }
        
        if let date = newsData["date"].string {
                   cell.lbDate.text = Date.convertDatetime(dateString: date)
        } else {
            cell.lbDate.text = ""
        }
        
        if let topic = newsData["topic"].string {
            cell.txtDetail.text = topic
        } else {
            cell.txtDetail.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DelayTime.delay(delay: 1) {
            
            let storyboard = UIStoryboard(name: "Main", bundle:nil)
            
            let newsDetailVC: DetialViewController = storyboard.instantiateViewController(withIdentifier: "DetialViewControllerId") as! DetialViewController
            
            let newsData = jsonFastList[indexPath.row]
            
            if let lastestImage = newsData["lastestImage"].string {
                
                var imagePath = IMAGE_URL
                imagePath += lastestImage
                
                newsDetailVC.lastest = imagePath
                
            } else {
                
            }
            
            if let topic = newsData["topic"].string {
                newsDetailVC.topic = topic
            } else {
                newsDetailVC.topic = ""
            }
            
            if let dateConvert = newsData["date"].string {
                
                newsDetailVC.date = dateConvert
                
            } else {
                newsDetailVC.date = ""
            }
            
            if let detial = newsData["detail"].string {
                newsDetailVC.detial = detial
            } else {
                newsDetailVC.detial = ""
            }
            self.navigationController?.pushViewController(newsDetailVC, animated: true)
        }
    }
    
}
