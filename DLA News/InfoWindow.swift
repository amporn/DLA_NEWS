//
//  InfoWindow.swift
//  DLA News
//
//  Created by Aom on 3/17/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit

class InfoWindow: UIView {
   
    @IBOutlet var lbTitle: UILabel!
    @IBOutlet var lbDate: UILabel!
    @IBOutlet var lbDetail: UILabel!
   
}
