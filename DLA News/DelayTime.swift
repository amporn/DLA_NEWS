//
//  DelayTime.swift
//  DLA News
//
//  Created by Aom on 3/14/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit

class DelayTime: NSObject {
    
    class func delay(delay:Double,closure:@escaping ()-> ()) {
        
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
}
