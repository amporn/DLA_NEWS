//
//  NewsViewController.swift
//  DLA News
//
//  Created by Aom on 3/10/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import Foundation

class GeneralViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableNews: UITableView!
    
    var newsTbCellNib = UINib(nibName: "NewTableviewCell", bundle:nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSlideMenuButton()
        self.tableNews.register(newsTbCellNib, forCellReuseIdentifier: "NewTableviewCell")
        
        callServiceNews()
        
    }
    
    func callServiceNews() {
        
        ConnectGeneral.servicesGeneral(success: {(response) -> Void in
            
            self.tableNews.reloadData()
            
        }) {(message) -> Void in
            
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonGeneralList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:NewsTableViewCell = tableNews.dequeueReusableCell(withIdentifier: "NewTableviewCell") as! NewsTableViewCell
        
        let newsData = jsonGeneralList[indexPath.row]
        
        if let lastestImage = newsData["lastestImage"].string {
            var imagePath = IMAGE_URL
            imagePath += lastestImage
            let imgURL = NSURL(string:imagePath)
            
            cell.imgNews.sd_setImage(with: imgURL as URL?, placeholderImage: UIImage(named:"placeholder"))
            
        } else {
            
        }
        
        if let dateConvert = newsData["date"].string {
            
            cell.lbDate.text = Date.convertDatetime(dateString: dateConvert)
            
        } else {
            cell.lbDate.text = ""
        }
        
        if let topic = newsData["topic"].string {
            cell.txtDetail.text = topic
        } else {
            cell.txtDetail.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DelayTime.delay(delay: 1) {
            
            let storyboard = UIStoryboard(name: "Main", bundle:nil)
            
            let newsDetailVC: DetialViewController = storyboard.instantiateViewController(withIdentifier: "DetialViewControllerId") as! DetialViewController
            
            let newsData = jsonGeneralList[indexPath.row]
            
            if let lastestImage = newsData["lastestImage"].string {
                
                var imagePath = IMAGE_URL
                imagePath += lastestImage
                
                newsDetailVC.lastest = imagePath
                
            } else {
                
            }
            
            if let topic = newsData["topic"].string {
                newsDetailVC.topic = topic
            } else {
                newsDetailVC.topic = ""
            }
            
            if let dateConvert = newsData["date"].string {
                
                newsDetailVC.date = dateConvert
                
            } else {
                newsDetailVC.date = ""
            }
            
            if let detial = newsData["detail"].string {
                newsDetailVC.detial = detial
            } else {
                newsDetailVC.detial = ""
            }
            self.navigationController?.pushViewController(newsDetailVC, animated: true)
        }
    }
    
}























